import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { FileChooser } from "@ionic-native/file-chooser";
import { FileTransfer, FileUploadOptions, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  url: string;

  constructor(
    public navCtrl: NavController,
    private transfer: FileTransfer,
    private file: File,
    private fileChooser: FileChooser,
    private camera: Camera
  ) {}

  open() {
    this.fileChooser
      .open()
      .then(uri => {
        this.url = uri;
        alert(this.url);
      })
      .catch(e => {
        alert(e);
      });
  }

  fileTransfer: FileTransferObject = this.transfer.create();


  upload() {
    let options: FileUploadOptions = {
       fileKey: 'file'
    }

    this.fileTransfer.upload(this.url, this.file.dataDirectory)
     .then((data) => {
       alert('si se guardó y no se en donde... Me aparece esta data: ' + data );
       console.log(data);
     }, (err) => {
       console.log('error: ' + err);
     })
  }

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: true,
    targetWidth:200,
    targetHeight:200
  }
  tomarFoto(){
  this.camera.getPicture(this.options).then((imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64:
    let base64Image = 'data:image/jpeg;base64,' + imageData;
    alert('La foto se ha tomado correctamente.');
   }, (err) => {
    alert('Ocurrió un error. ¿De pronto usted canceló?')
   });
  }
}
